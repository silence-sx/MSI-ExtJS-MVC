Ext.define("MSI.store.UserGridStore", {
			extend : 'Ext.data.Store',
			fields : ['name', 'email', 'phone'],
			autoLoad : false,
			data : {
				'items' : [{
							'name' : '张三',
							"email" : "lisa@geekice.com",
							"phone" : "555-111-1224"
						}, {
							'name' : '李四',
							"email" : "bart@geekice.com",
							"phone" : "555-222-1234"
						}, {
							'name' : '王五',
							"email" : "home@geekice.com",
							"phone" : "555-222-1244"
						}, {
							'name' : '赵六',
							"email" : "marge@geekice.com",
							"phone" : "555-222-1254"
						}]
			},
			proxy : {
				type : 'memory',
				reader : {
					type : 'json',
					root : 'items'
				}
			}
		});