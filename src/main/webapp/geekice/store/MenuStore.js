Ext.define("MSI.store.MenuStore", {
			extend : 'Ext.data.TreeStore',
			root : {
				expanded : false,
				children : [{
							text : "人员列表",
							expanded : false,
							children : [{
										text : "在职员工",
										leaf : true
									}, {
										text : "离职员工",
										leaf : true
									}]
						}, {
							text : "测试菜单",
							leaf : true
						}]
			}
		});