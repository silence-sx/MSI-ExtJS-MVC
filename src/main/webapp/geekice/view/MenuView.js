Ext.define('MSI.view.MenuView', {
			extend : 'Ext.tree.Panel',
			alias : 'widget.menuPanel',
			border : false,
			rootVisible : false,
			store : Ext.create('MSI.store.MenuStore'),
			viewConfig : {
				loadingText : "正在加载..."
			},
			initComponent : function() {
				this.callParent(arguments);
			}
		});