Ext.define("MSI.view.West", {
			extend : 'Ext.panel.Panel',

			width : 200,
			defaults : {
				bodyPadding : 0
			},
			initComponent : function() {
				Ext.apply(this, {
							layout : 'fit',
							collapsible:true,
							border: false,
							split: true,
							title:'系统模块',
							items:[Ext.create('MSI.view.AccordionView')]
						});
				this.callParent();
			}
		});