Ext.define("MSI.view.AccordionView", {
	extend : 'Ext.panel.Panel',
	initComponent : function() {
		Ext.apply(this, {
					width : 250,
					layout : {
						type : 'accordion',
						titleCollapse : true,
						animate : true,
						border : false
					},
					items : [{
								title : '员工管理',
								items : [Ext.create('MSI.view.MenuView')]
							}, {
								title : '部门管理',
								html : 'Panel content!'
							}, {
								title : '系统设置',
								html : 'Panel content!'
							}]

				});
		this.callParent();
	}
});