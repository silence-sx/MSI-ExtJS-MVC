// 客户Panel中Grid
Ext.define('MSI.view.UserGridView', {
			extend : 'Ext.grid.Panel',
			alias : 'widget.UserGrid',
			initComponent : function() {
				var store = Ext.create("MSI.store.UserGridStore");
				Ext.apply(this, {
							width : '100%',
							stripeRows : true,
							closable : true,
							store : store,
							columns : [{
										text : 'Name',
										dataIndex : 'name'
									}, {
										text : 'Email',
										dataIndex : 'email',
										flex : 1
									}, {
										text : 'Phone',
										dataIndex : 'phone'
									}],

							dockedItems : [{
										xtype : 'pagingtoolbar',
										store : store,
										dock : 'bottom',
										displayInfo : true
									}]
						});
				this.callParent(arguments);
			}
		});