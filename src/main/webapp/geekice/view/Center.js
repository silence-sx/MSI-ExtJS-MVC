Ext.define("MSI.view.Center", {
			extend : 'Ext.tab.Panel',
			alias : 'widget.centerTabs',
			initComponent : function() {
				Ext.apply(this, {
							border : false,
							tabBar : {
								defaults : {
									height : 25
								}
							},
							items : [

							{
										title : 'Hello World MSI',
										html : '<h1>Hello World!</h1> ExtJS MVC 管理系统界面(Mange System Interface)'
									}]
						});
				this.callParent(arguments);
			}
		});