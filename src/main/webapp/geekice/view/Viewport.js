Ext.define("MSI.view.Viewport", {
			extend : 'Ext.container.Viewport',

			initComponent : function() {
				Ext.apply(this, {
							layout : 'border',
							items : [Ext.create('MSI.view.Header', {
												region : 'north'
											}), Ext.create('MSI.view.Bottom', {
												region : 'south'
											}), Ext.create('MSI.view.West', {
												region : 'west'
											}), Ext.create('MSI.view.Center', {
												region : 'center'
											})]
						});
				this.callParent(arguments);
			}

		});