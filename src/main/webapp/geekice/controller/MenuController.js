Ext.define('MSI.controller.MenuController', {
			extend : 'Ext.app.Controller',
			init : function() {
				this.control({
							'menuPanel' : {
								itemclick : this.clickMenu
							}
						});
			},
			// 用户点击菜单出发事件
			clickMenu : function(view, record, item, index, e, eOpts) {
				
				//如果为叶子,点击创建tab
				if(record.raw.leaf){
					var tabs = view.up("viewport").down("centerTabs");
					var tabItem=Ext.create('MSI.view.UserGridView',{
					 title:record.raw.text
					});
					tabs.add(tabItem);
					tabs.setActiveTab(tabItem);
					
				}
 
			}
		});