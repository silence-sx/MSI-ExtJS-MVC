/**
 * @description 配置信息
 * @author Silence
 * @create 2013/12/18
 */
Ext.onReady(function() {
			// 开启悬浮提示功能
			Ext.QuickTips.init();
			// 开启动态加载
			Ext.Loader.setConfig({
						enabled : true
					});
			// 创建应用程序的实例
			Ext.application({
						// 设定命名空间
						autoCreateViewport : true,
						name : 'MSI',
						appFolder : 'geekice',
						 controllers : ['MenuController'],
						launch : function() {
						}
					});

		});