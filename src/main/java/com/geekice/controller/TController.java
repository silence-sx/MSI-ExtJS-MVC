package com.geekice.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @ClassName: TController
 * @Description: TODO
 * @author: SilenceSu
 * @modify.Date: 2013年12月17日 下午6:31:47
 */

@Controller
@RequestMapping("t")
public class TController {

	@RequestMapping("t1")
	@ResponseBody
	public String t1() {
		return "test page";
	}
}
